# pluralsight course Creating your first Spring Boot application

* https://app.pluralsight.com/library/courses/spring-boot-first-application/table-of-contents

## git repo

* https://bitbucket.org/rachu/pluralsight-dasboot

## dependencies

* spring boot 2.0.2
* hikariCP
* flyway
* h2 database

## url for webapp

* http://localhost:8080/index.html#/shipwrecks

## problem with hikari jdbc-url

After upgrading spring-boot-starter to version 2.0.0 or greater you will get a 
jdbc-url error.  Here is my fix.

Before:

spring.datasource.url=jdbc:h2:file:~/dasboot

After: 

spring.datasource.jdbc-url=jdbc:h2:file:~/dasboot



Reference: https://stackoverflow.com/a/49141541/406886

