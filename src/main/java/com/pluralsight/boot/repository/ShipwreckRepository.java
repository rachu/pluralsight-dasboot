package com.pluralsight.boot.repository;


import com.pluralsight.boot.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;



public interface ShipwreckRepository extends JpaRepository<Shipwreck, Long> {

}
