package com.pluralsight.boot;

import com.pluralsight.boot.model.Shipwreck;
import com.pluralsight.boot.repository.ShipwreckRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = App.class)
@WebAppConfiguration

public class ShipwreckRepositoryIntegrationTest {

    @Autowired
    private ShipwreckRepository shipwreckRepository;

@Test
    public void testFindAll() {
    List<Shipwreck> wrecks = shipwreckRepository.findAll();
    assertThat(wrecks.size(), is(greaterThanOrEqualTo(0)));
}
}
