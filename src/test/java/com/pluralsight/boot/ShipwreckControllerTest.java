package com.pluralsight.boot;

import com.pluralsight.boot.controller.ShipwreckController;
import com.pluralsight.boot.model.Shipwreck;
import com.pluralsight.boot.repository.ShipwreckRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ShipwreckControllerTest {

    @InjectMocks
    private ShipwreckController sc;

    @Mock
    private ShipwreckRepository shipwreckRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testShipwreckGet() {

        Shipwreck sw = new Shipwreck();
        sw.setId(1L);
        when(shipwreckRepository.getOne(1L)).thenReturn(sw);

        Shipwreck wreck = sc.get(1L);

        verify(shipwreckRepository).getOne(1L);

        //assertEquals(1L, wreck.getId().longValue());

        assertThat(wreck.getId(),is(1L));

    }
}
