package com.pluralsight.boot;

import static org.junit.Assert.assertEquals;

import com.pluralsight.boot.controller.HomeController;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{

    @Test
    public void testApp()
    {
        HomeController hc = new HomeController();
        String result = hc.home();
        assertEquals(result,"Das Boot, reporting for duty!");
    }


}
